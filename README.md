# jl-cloud-coursework
tested on a VM with 32GB of storage and 4GB of RAM
VM had a NAT adapter and a host-only adapater

to start application 
1. clone git repository into root
2. give permission to run docker.sh file (chmod 775 ./jl-cloud-coursework/docker.sh)
3. run command './jl-cloud-coursework/docker.sh'

to test application
1. run command sudo docker container ls. all relevant containers should be lsited
2. ping each container from inside other containers. Should recieve packets back from each container
