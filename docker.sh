sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
sudo apt install docker-ce
mkdir php
mkdir php/src
mkdir compose
mkdir compose/nodeJS
mkdir nginx
touch nginx/nginx.conf
printf 'upstream nodeapp{\nserver 192.168.56.109:81;\nserver 192.168.56.109:82;\nserver 192.168.56.109:83;\n}\nserver {\nlisten 80;\nlocation / {proxy_pass http://nodeapp;/n}/n}' > nginx/nginx.conf
touch nginx/Dockerfile
touch nginx/docker-compose.yaml
printf 'FROM nginx\nRUN webserm /etc/nginx/conf.d/default.conf\nCOPY nginx.conf /etc/nginx/conf.d/default.conf' > nginx/Dockerfile
printf 'version: "3" \nservices:\n   node1:\n        build: ./nodejs\n      ports: - "81:3000"\n   node2:\n      build: ./nodejs\n      ports: - "82:3000"\n   node3:build: ./nodejs\n      ports: - "83:3000"\n   nginx:\n      build: ./nginx\n      ports: - "80:80"\n   depends_on:\n - webservicetier1\n - webservicetier2\n - webservicetier3' > nginx/docker-compose.yaml
touch php/Dockerfile
printf 'FROM php:7.2-apache \nCOPY src/ /var/www/html/' > php/Dockerfile
touch ./php/src/index.php
printf '<? echo "hello world" ?>' > php/src/index.php
touch ./compose/nodeJS/Dockerfile
cd compose/nodeJS && git clone https://gitlab.com/JordanScottLee/jl-cloud-week1
printf 'FROM alpine\nRUN apk add --update nodejs npm\nWORKDIR /usr/src/app\nEXPOSE 3000\nCOPY jl-cloud-week1/ .\nRUN npm install\nCMD [ "node", "test.js" ]' > Dockerfile
sudo docker build -t webtierimage .
sudo apt install lynx
sudo docker run -d -p 81:3000 --name webserviceTier1 webtierimage
sudo docker run -d -p 82:3000 --name webserviceTier2 webtierimage
sudo docker run -d -p 83:3000 --name webserviceTier3 webtierimage